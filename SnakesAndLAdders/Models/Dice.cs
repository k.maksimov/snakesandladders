﻿using SnakesAndLadders.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders
{
    public static class Dice
    {
        private static readonly Random rnd = new Random();
        public static int Throw() {
            int throwDice;
            throwDice = rnd.Next(1,6);
            return throwDice;
        }
        
    }
}
