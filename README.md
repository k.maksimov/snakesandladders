# SnakesAndLadders
![ALT](/images/logo.png)


## Description

A simple single player implementation of the famous Snakes and Ladders board game. I used this project to exercise the concept of movement in matrices. 
Just throw the dice until you win.
## Gameplay Footage
![ALT](/images/snl.png)

