﻿using SnakesAndLadders.Models;
using SnakesAndLadders.Visuals;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders
{
    public class Engine
    {
        public void Start()
        {
            // start game
            Display.TitleScreen();

            bool gameInProcess = true;
            int totalThrows = 0;
            string inputStart = Display.StartMenu();

            if (inputStart.ToLower() == "n") //End game lose check
            {
                Display.EndScreen();
                return;
            }

            Display.LevelSelectScreen("======================");//

            while (gameInProcess)
            {
                
                try
                {
                    // game setup
                    int.TryParse(Console.ReadLine(), out int level);//parse level input
                    IBoard board = LevelSelector.SelectLevel(level);//initialize board
                    
                    // main game loop - start playing the selected level, play until the final implemented level is reached

                    while (level <= Constants.finishedLevelsCount) {
                        Engine.Setup(board);//setup visuals
                        
                        while (!board.IsGameComplete)
                        {
                            Engine.Play(board,level); // play current level
                        }
                        totalThrows += board.TurnCounter;
                        level++;
                        if (level <= Constants.finishedLevelsCount) {
                            board = LevelSelector.SelectLevel(level);
                        }
                        
                    }

                    gameInProcess = false;
                    Display.EndScreen(totalThrows);//endgame win screen
                }
                catch (NotImplementedException ex)
                {
                    Display.LevelSelectScreen(ex.Message);
                }
                catch (FormatException ex)
                {
                    Display.LevelSelectScreen(ex.Message);
                }

            }
        }
        public static void Setup(IBoard board)
        {
            Display.Clear();//clear screen to setup start screen
            Display.Logo();// display logo visuals
            Display.Board(board); //display draw board visuals
            Display.Ready();// display Ready visuals;
            Display.ProgressBar(board); //display progress bar
            Console.ReadLine();//wait input for next turn;
        }
        public static void Play(IBoard board,int level)
        {
            Display.Clear();//clear screen to draw next iteration of the game state
            Display.Logo();// display logo visuals

            board.Throw();// throw a dice
            board.Move();// move according to the throw dice result

            Display.Board(board);// display draw board visuals
            Display.Dice(board.ThrowDice);// display draw dice visuals
            Display.Status(board);// display progress messages visualsc
            Display.ProgressBar(board);// display progress bar
            Engine.AwaitInput(board,level);
            Console.ReadLine();//wait for input
        }
        public static void AwaitInput(IBoard board,int level) {
            if (board.IsGameComplete)
            {
                Console.WriteLine($"You have completed level {level} in {board.TurnCounter} throws! Press enter to continue:");
            }
            else
            {
                Console.WriteLine("Please press enter to throw the dice.");
            }
        }
    }
}
