﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders.Models
{
    public class BoardLevelThree : Board
    {
        #region constructor
        public BoardLevelThree() : base()
        {

        }
        #endregion constructor

        #region methods
        public override string[,] Level(string player)
        {
            string[,] board =
                {
            {"_","__","_","__","_","__","_", "__", "_", "__", "_", "__", "_", "__", "_", "__", "_", "__", "_", "__","_",  "_" },
            {"1","00"," ","99"," ","98"," ", " \\", " ", "96", " ", "95", " ", "94", " ", "93", " ", "92", " ", "91"," ", "|" },
            {"|","81"," ","82"," ","83"," ", "84", "\\", "85", " ", "86", " ", "87", " ", "88", "\\", "89"," ","90"," ", "|" },
            {"|","80"," ","79"," ","78"," ", "77", " ", "\\_", " ", "75", " ", "74", " ", "73", " ", "\\ ", " ", "71"," ", "|" },
            {"|","61"," ","62"," ","63"," ", "64", " ", "65", " ", "66", " ", "67", " ", "68", " ", " \\", " ", "70"," ",  "|" },
            {"|","60"," ","59"," ","58"," ", "57", " ", "56", " ", "55", " ", "54", " ", "53", " ", "52", "\\", "_ "," ",  "|" },
            {"|","41"," ","42"," "," \\"," ", "44", " ", "45", " ", "46", " ", "47", " ", "48", " ", "49", " ", "50"," ",  "|" },
            {"|","40"," ","39"," ","38","\\", "_ ", " ", "36", " ", "35", " ", "34", " ", "33", " ", "32", " ", "31"," ",  "|" },
            {"|","21"," ","22"," ","23"," ", "24", " ", "25", " ", "26", "\\", "27", " ", "28", " ", "29", " ", "30"," ",  "|" },
            {"|","20"," ","19"," ","18"," ", "17", " ", "16", " ", "15", " ", "\\_", " ", "13", " ", "12", " ", "11"," ",  "|" },
            {"|",player," ","02"," ","03"," ", "04", " ", "05", " ", "06", " ", "07", " ", "08", " ", "09", " ", "10"," ",  "|" },
            {"T","TT","T","TT","T","TT","T", "TT", "T", "TT", "T", "TT", "T", "TT", "T", "TT", "T", "TT", "T", "TT","T",  "T" }
            };
            return board;

        }

        // move magic
        public override void Move()
        {

            //ladder or snake case check
            int chk = CurrentPosition + ThrowDice;

            //snake case positions
            if (chk == 26 || chk == 43 || chk == 88 || chk == 97)
            {


                if (chk == 26)
                {
                    CurrentPosition = 14;
                    GamePiece.TurnLeft();
                    State[Row, Col] = OldCell;
                    OldCell = State[9, 13];
                    State[9, 13] = GamePiece.Image;
                    Row = 9;
                    Col = 13;
                }
                else if (chk == 43)
                {
                    CurrentPosition = 37;
                    GamePiece.TurnLeft();
                    State[Row, Col] = OldCell;
                    OldCell = State[7, 7];
                    State[7, 7] = GamePiece.Image;
                    Row = 7;
                    Col = 7;
                }
                else if (chk == 88)
                {
                    CurrentPosition = 51;
                    GamePiece.TurnLeft();
                    State[Row, Col] = OldCell;
                    OldCell = State[5, 19];
                    State[5, 19] = GamePiece.Image;
                    Row = 5;
                    Col = 19;
                }
                else if (chk == 97)
                {
                    CurrentPosition = 76;
                    GamePiece.TurnLeft();
                    State[Row, Col] = OldCell;
                    OldCell = State[3, 9];
                    State[3, 9] = GamePiece.Image;
                    Row = 3;
                    Col = 9;
                }
                this.BoardMessage = String.Format(Constants.boardMessageSnake, CurrentPosition);
                //Console.WriteLine(String.Format("Snake! Going to {0}",CurrentPosition));
            }
            //else case - standard movement with no "teleportation", base move
            else
            {
                base.Move();
            }

            return;
        }
        #endregion methods
    }
}
