﻿using SnakesAndLadders.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders.Models
{
    public abstract class Board : IBoard
    {
        #region fields
        private GamePiece gamePiece;
        private int throwDice;
        private int turnCounter;
        private int currentPosition;
        private int oldPosition;
        private string oldCell;
        private int row;
        private int col;
        private bool isDirectionRight;
        private string[,] state;
        private string boardMessage;
        private bool isGameComplete;

        #endregion fields

        #region constructor
        protected Board()
        {
            this.Row = Constants.StartRow;
            this.Col = Constants.StartCol;
            this.TurnCounter = Constants.TurnCounterStartValue;
            this.CurrentPosition = Constants.CurrentPositionStartValue;
            this.OldCell = Constants.OldCellStartValue;
            this.IsDirectionRight = Constants.IsDirectionRightStartValue;
            this.GamePiece = new GamePiece(Constants.GamePieceIsRightVisuals, Constants.GamePieceIsLeftVisuals);
            this.State = Level(GamePiece.Image);
            this.BoardMessage = Constants.boardMessageDefault;
            this.IsGameComplete = false;
        }
        #endregion constructor

        #region properties
        public int ThrowDice { get { return this.throwDice; } private set { this.throwDice = value; } }
        public int TurnCounter { get { return this.turnCounter; } private set { this.turnCounter = value; } }
        public string[,] State { get { return this.state; } private set { this.state = value; } }
        public bool IsGameComplete { get { return this.isGameComplete; }protected set { this.isGameComplete = value; } }
        public string BoardMessage { get { return this.boardMessage; }protected set { this.boardMessage = value; } }
        public int CurrentPosition { get { return this.currentPosition; } protected set { this.currentPosition = value; } }
        public int OldPosition { get { return this.oldPosition; } protected set { this.oldPosition = value; } }
        public string OldCell { get { return this.oldCell; } protected set { this.oldCell = value; } }
        public int Row { get { return this.row; } protected set { this.row = value; } }
        public int Col { get { return this.col; } protected set { this.col = value; } }
        public bool IsDirectionRight { get { return this.isDirectionRight; } protected set { this.isDirectionRight = value; } }
        public GamePiece GamePiece { get { return this.gamePiece; } protected set { this.gamePiece = value; } }
        

        #endregion properties

        #region methods
        public void Throw()
        {
            // throwing the dice functionality
            this.ThrowDice = Dice.Throw();
            // count turns
            this.TurnCounter++;
        }
        public abstract string[,] Level(string player);
        public virtual void Move()
        {
            this.boardMessage = Constants.boardMessageDefault;
            OldPosition = CurrentPosition;

            for (int i = 1; i <= ThrowDice; i++)
            {

                CurrentPosition++;
                if (CurrentPosition >= 100)
                {
                    State[Row, Col] = oldCell;
                    OldCell = State[1, 1];
                    State[1, 1] = GamePiece.Image;
                    Row = 1;
                    Col = 1;
                    this.IsGameComplete = true;
                    break;

                }

                if (GamePiece.IsRight)
                {
                    GamePiece.TurnRight();
                    if (Col >= 18)
                    {
                        GamePiece.TurnLeft();
                        State[Row, Col] = OldCell;
                        OldCell = State[Row - 1, Col];
                        State[Row - 1, Col] = GamePiece.Image;
                        Row--;
                    }
                    else
                    {
                        State[Row, Col] = OldCell;
                        OldCell = State[Row, Col + 2];
                        State[Row, Col + 2] = GamePiece.Image;
                        Col += 2;
                    }

                }
                else if (!GamePiece.IsRight)
                {
                    GamePiece.TurnLeft();
                    if (Row % 2 == 1 && Col == 1)
                    {
                        GamePiece.TurnRight();
                        State[Row, Col] = OldCell;
                        OldCell = State[Row - 1, Col];
                        State[Row - 1, Col] = GamePiece.Image;
                        Row--;

                    }
                    else
                    {
                        if (Col < 2)
                        {
                            GamePiece.TurnRight();
                            break;
                        }
                        State[Row, Col] = OldCell;
                        OldCell = State[Row, Col - 2];
                        State[Row, Col - 2] = GamePiece.Image;
                        Col -= 2;
                    }

                }

            }
        }

        #endregion methods
    }
}
