﻿using SnakesAndLadders.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders.Models
{
    public interface IBoard
    {
        public string BoardMessage{ get; }
        public int ThrowDice { get;}
        public int TurnCounter { get;}
        public int CurrentPosition { get; }
        public int OldPosition { get; }
        public string OldCell { get; }
        public int Row { get; }
        public int Col { get; }
        public bool IsDirectionRight { get; }
        public GamePiece GamePiece { get; }
        public string[,] State { get; }
        public bool IsGameComplete { get; }

        public void Throw();
        public string[,] Level(string player);
        public void Move();
    }
}
