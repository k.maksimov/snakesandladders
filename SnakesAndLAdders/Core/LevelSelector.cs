﻿using SnakesAndLadders.Models;
using SnakesAndLadders.Visuals;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders
{
    public static class LevelSelector
    {
        public static Board SelectLevel(int level) {
            switch (level)
            {
                case 1:
                    return new BoardLevelOne();
                case 2:
                    return new BoardLevelTwo();
                case 3:
                    return new BoardLevelThree();
                case 4:
                    throw new NotImplementedException($"Level {level} is not implemented yet!");
                default:
                    throw new NotImplementedException("Invalid level");
            }
        }
    }
}
