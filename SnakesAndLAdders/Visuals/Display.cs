﻿using SnakesAndLadders.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders.Visuals
{
    public static class Display
    {
        
        #region service methods
        public static void Clear() {
            Console.Clear();
        }
        public static void DrawRows(int num) {
            for (int i = 0; i < num; i++)
            {
                for (int j = 0; j < 59; j++) {
                    if (i % 2 == 0)
                    {
                        Display.ChangeColor(ConsoleColor.Red);
                        Console.Write("=");
                        Display.ChangeColor(ConsoleColor.Yellow);
                        Console.Write("O");
                    }
                    else {

                        Display.ChangeColor(ConsoleColor.Yellow);
                        Console.Write("O");
                        Display.ChangeColor(ConsoleColor.Red);
                        Console.Write("=");
                    }
                }
                Console.WriteLine();
                
            }
            Display.ChangeColor(Constants.defaultForegroundColor);
        }
        public static void ChangeColor(ConsoleColor color) {
            Console.ForegroundColor = color;
        }
        public static void ChanceBackground(ConsoleColor color) {
            Console.BackgroundColor = color;
        }

        #endregion service methods

        #region game state elements
        public static void Dice(int throwDice)
        {
            Display.ChangeColor(Constants.defaultDiceColor);
            switch (throwDice)
            {
                case 1:
                    Console.WriteLine(" _____");
                    Console.WriteLine("|     |");
                    Console.WriteLine("|  *  |");
                    Console.WriteLine("|_____|");
                    break;
                case 2:
                    Console.WriteLine(" _____");
                    Console.WriteLine("|  *  |");
                    Console.WriteLine("|     |");
                    Console.WriteLine("|__*__|");
                    break;
                case 3:
                    Console.WriteLine(" _____");
                    Console.WriteLine("|    *|");
                    Console.WriteLine("|  *  |");
                    Console.WriteLine("|*____|");
                    break;
                case 4:
                    Console.WriteLine(" _____");
                    Console.WriteLine("|*   *|");
                    Console.WriteLine("|     |");
                    Console.WriteLine("|*___*|");
                    break;
                case 5:
                    Console.WriteLine(" _____");
                    Console.WriteLine("|*   *|");
                    Console.WriteLine("|  *  |");
                    Console.WriteLine("|*___*|");
                    break;
                case 6:
                    Console.WriteLine(" _____");
                    Console.WriteLine("|*   *|");
                    Console.WriteLine("|*   *|");
                    Console.WriteLine("|*___*|");
                    break;
                default:
                    Console.WriteLine("First Throw!");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine();
                    break;
            }
            Display.ChangeColor(Constants.defaultForegroundColor);
        }
        public static void Board(IBoard board)
        {
            Display.ChangeColor(Constants.defaultBoardColor);
            for (int i = 0; i < board.State.GetLength(0); i++)
            {
                Console.Write(Constants.pad);

                for (int j = 0; j < board.State.GetLength(1); j++)
                {
                    if (board.State[i, j] == board.GamePiece.Image)
                    {
                        Display.ChangeColor(Constants.defaultPlayerColor);
                        Console.Write(board.State[i, j]);
                        Display.ChangeColor(Constants.defaultBoardColor);
                    }
                    else if (board.State[i, j].Trim() == "/" || board.State[i, j] == "_/") {
                        Display.ChangeColor(Constants.defaultLadderColor);
                        Console.Write(board.State[i, j]);
                        Display.ChangeColor(Constants.defaultBoardColor);
                    }
                    else if (board.State[i, j].Trim() == "\\" || board.State[i, j] == "\\_"||board.State[i,j].Trim()=="_"&&i!=0) {
                        Display.ChangeColor(Constants.defaultSnakeColor);
                        Console.Write(board.State[i, j]);
                        Display.ChangeColor(Constants.defaultBoardColor);
                    }

                    else if (int.TryParse(board.State[i, j], out int result)) {
                        Display.ChangeColor(Constants.defaultForegroundColor);
                        Console.Write(board.State[i, j]);
                        Display.ChangeColor(Constants.defaultForegroundColor);
                    }

                    else {
                        Display.ChangeColor(Constants.defaultBoardColor);
                        Console.Write(board.State[i, j]);
                        Display.ChangeColor(Constants.defaultForegroundColor);
                    }
                    
                }
                Console.WriteLine();
            }
            Display.ChangeColor(Constants.defaultForegroundColor);
        }
        public static void Status(IBoard board)
        {
            
            if (board.CurrentPosition < 100)
            {
                Console.WriteLine(board.BoardMessage);
                Console.WriteLine(String.Format(Constants.progressMessage, board.OldPosition, Math.Min(board.CurrentPosition, 100)));
            }
            else {
                Console.WriteLine("WOW!!!");
                Console.WriteLine($"You rolled {board.ThrowDice}, just enough to complete the level!!!");
            }
            Console.WriteLine($"Dice throws: {board.TurnCounter}");
        }
        public static void ProgressBar(IBoard board) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Game is {Math.Min(board.CurrentPosition,100)}% complete.");
            for (int i = 1; i <= 100; i++) {
                if (i <= board.CurrentPosition)
                {
                    sb.Append("|");
                }
                else {
                    sb.Append(".");
                }
            }
            Display.ChangeColor(Constants.defaultProgressBarColor);
            Console.WriteLine(sb.ToString());
            Display.ChangeColor(Constants.defaultForegroundColor);

        }
        #endregion game state elements

        #region menu elements and visuals

        public static void Logo(int miliseconds)
        {
            StringBuilder sb = new StringBuilder();
            Display.ChangeColor(Constants.defaultMessageColor);
            sb.AppendLine("  _________              __                                       .___   .__              .___  .___                   ");
            sb.AppendLine(" /   _____/ ____ _____  |  | __ ____   ______   _____   ____    __| _/   |  | _____     __| _/__| _/___________  ______");
            sb.AppendLine(" \\_____  \\\\/    \\\\__  \\ |  |/ // __ \\ /  ___/   \\__  \\ /    \\  / __ |    |  | \\__  \\   / __ |/ __ |/ __ \\_  __ \\/  ___/");
            sb.AppendLine(" /        \\   |  \\/ __ \\|    <\\  ___/ \\___ \\     / __ \\|  |  \\/ /_/ |    |  |__/ __ \\_/ /_/ / /_/ \\  ___/|  | \\/\\___ \\ ");
            sb.AppendLine("/_________/___|  (___  /___|_ \\\\___/ /_____/    (___  /|__|  /\\_____|    |____(____  /\\_____\\_____|\\___/ |__|  /_____/");
            sb.AppendLine("               \\/    \\/      \\/                     \\/     \\/                      \\/               ");
            string result = sb.ToString();
            foreach (var ch in result) {
                Console.Write(ch);
                System.Threading.Thread.Sleep(miliseconds);
            }
            
            Display.ChangeColor(Constants.defaultForegroundColor);
            //source - https://patorjk.com/software/taag
        }
        public static void Logo()
        {
            Display.ChangeColor(Constants.defaultMessageColor);
            Console.WriteLine("  _________              __                                       .___   .__              .___  .___                   ");
            Console.WriteLine(" /   _____/ ____ _____  |  | __ ____   ______   _____   ____    __| _/   |  | _____     __| _/__| _/___________  ______");
            Console.WriteLine(" \\_____  \\\\/    \\\\__  \\ |  |/ // __ \\ /  ___/   \\__  \\ /    \\  / __ |    |  | \\__  \\   / __ |/ __ |/ __ \\_  __ \\/  ___/");
            Console.WriteLine(" /        \\   |  \\/ __ \\|    <\\  ___/ \\___ \\     / __ \\|  |  \\/ /_/ |    |  |__/ __ \\_/ /_/ / /_/ \\  ___/|  | \\/\\___ \\ ");
            Console.WriteLine("/_________/___|  (___  /___|_ \\\\___/ /_____/    (___  /|__|  /\\_____|    |____(____  /\\_____\\_____|\\___/ |__|  /_____/");
            Console.WriteLine("               \\/    \\/      \\/                     \\/     \\/                      \\/               ");
            Display.ChangeColor(Constants.defaultForegroundColor);
            //source - https://patorjk.com/software/taag
        }
        public static void Ready() {

            Display.ChangeColor(Constants.defaultMessageColor);
            Console.WriteLine("  _____    __    ___              __     __");
            Console.WriteLine(" / ___/__ / /_  / _ \\___ ___ ____/ /_ __/ /");
            Console.WriteLine("/ (_ / -_) __/ / , _/ -_) _ `/ _  / // /_/ ");
            Console.WriteLine("\\___/\\__/\\__/ /_/|_|\\__/\\_,_/\\_,_/\\_, (_)  ");
            Console.WriteLine("                                 /___/     ");
            Display.ChangeColor(Constants.defaultForegroundColor);
            Display.DrawRows(2);
        }
        public static void TitleScreen()
        {
            Display.ChanceBackground(Constants.defaultBackgroundColor);
            Display.ChangeColor(Constants.defaultForegroundColor);
            Display.Logo(1);
            Display.DrawRows(14);// Display n rows of filler visuals, screen just looks empty at that point;
        }
        public static string StartMenu()
        {

            Console.WriteLine("Do you want to play? Y/N");
            Console.WriteLine("Yes - type y");
            Console.WriteLine("No - type n");
            Console.WriteLine("Awaiting input:");

            string inputStart = Console.ReadLine();

            while (inputStart != "Y" && inputStart != "N" && inputStart != "y" && inputStart != "n")
            {
                switch (inputStart)
                {   
                    default:
                        Console.WriteLine("Invalid input!");
                        inputStart = Console.ReadLine();
                        break;
                }
            }
            return inputStart;
        }
        public static void LevelSelectScreen(string message)
        {
            Console.Clear();
            Display.Logo();
            Display.DrawRows(14);// Display n rows of filler visuals, screen just looks empty at that point;
            Console.WriteLine(message);
            
            Console.WriteLine("Select a level: ");
            int index = 1;
            foreach(string str in Constants.CompletedLevels) {
                Console.WriteLine($"{index} - {str}");
                index++;

            }

        }
        public static void Congratulations() {

            Display.ChangeColor(ConsoleColor.Yellow);
            Console.WriteLine(Constants.pad2+"  _____                        __       __     __  _               __");
            Console.WriteLine(Constants.pad2 + " / ___/__  ___  ___ ________ _/ /___ __/ /__ _/ /_(_)__  ___  ___ / /");
            Console.WriteLine(Constants.pad2 + "/ /__/ _ \\/ _ \\/ _ `/ __/ _ `/ __/ // / / _ `/ __/ / _ \\/ _ \\(_-</_/ ");
            Console.WriteLine(Constants.pad2 + "\\___/\\___/_//_/\\_, /_/  \\_,_/\\__/\\_,_/_/\\_,_/\\__/_/\\___/_//_/___(_)  ");
            Console.WriteLine(Constants.pad2 + "              /___/                                                  ");
            Display.ChangeColor(Constants.defaultForegroundColor);
        }
        public static void Congratulations(int miliseconds)
        {
            StringBuilder sb = new StringBuilder();
            Display.ChangeColor(Constants.defaultMessageColor);
            sb.AppendLine(Constants.pad2 + "  _____                        __       __     __  _               __");
            sb.AppendLine(Constants.pad2 + " / ___/__  ___  ___ ________ _/ /___ __/ /__ _/ /_(_)__  ___  ___ / /");
            sb.AppendLine(Constants.pad2 + "/ /__/ _ \\/ _ \\/ _ `/ __/ _ `/ __/ // / / _ `/ __/ / _ \\/ _ \\(_-</_/ ");
            sb.AppendLine(Constants.pad2 + "\\___/\\___/_//_/\\_, /_/  \\_,_/\\__/\\_,_/_/\\_,_/\\__/_/\\___/_//_/___(_)  ");
            sb.AppendLine(Constants.pad2 + "              /___/                                                  ");
            string result = sb.ToString();
            foreach (var ch in result)
            {
                Console.Write(ch);
                System.Threading.Thread.Sleep(miliseconds);
            }

            Display.ChangeColor(Constants.defaultForegroundColor);
        }
        public static void EndScreen()
        {
            Console.Clear();
            Console.WriteLine("E");
            Console.WriteLine("N");
            Console.WriteLine("D");
            Console.WriteLine("G");
            Console.WriteLine("A");
            Console.WriteLine("M");
            Console.WriteLine("E");
            Console.WriteLine("!");
        }
        public static void EndScreen(int turns)
        {
            
            Display.Clear();
            Display.Logo(1);
            Display.DrawRows(7);
            Display.Congratulations(1);
            Display.Congratulations();
            Display.Congratulations(1);
            Console.WriteLine($"You completed the game in {turns} throws!");
            Console.ReadLine();

            return;

        }

        #endregion menu elements and visuals
    }
}
