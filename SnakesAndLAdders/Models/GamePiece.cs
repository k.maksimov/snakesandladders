﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders.Models.Contracts
{
    public class GamePiece:IGamePiece
    {
        #region fields
        private readonly string isLeftImage;
        private readonly string isRightImage;

        private string image;
        private bool isRight;

        #endregion fields

        #region constructor
        public GamePiece(string isRightImage,string isLeftImage)
        {
            this.isLeftImage = isLeftImage;
            this.isRightImage = isRightImage;
            this.isRight = true;
            this.image = this.isRightImage;
        }
        #endregion constructor

        #region properties
        public string Image
        {
            get { return this.image; }
            private set { this.image = value; }
        }
        public bool IsRight
        {
            get { return this.isRight; }
            private set { this.isRight = value; }
        }
        #endregion properties

        #region methods
        public void TurnRight()
        {
            this.IsRight = true;
            this.Image = isRightImage;

        }
        public void TurnLeft()
        {
            this.IsRight = false;
            this.Image = isLeftImage;

        }
        #endregion methods
    }
}
