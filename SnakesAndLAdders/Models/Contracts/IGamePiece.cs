﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders.Models.Contracts
{
    public interface IGamePiece
    {
        public string Image { get; }
        public bool IsRight { get; }
        public void TurnRight();
        public void TurnLeft();


    }
}
