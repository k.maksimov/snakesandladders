﻿using System;

namespace SnakesAndLadders
{
    class Startup
    {
        static void Main(string[] args)
        {
            Engine engine = new Engine();
            engine.Start();
        }

    }
}
