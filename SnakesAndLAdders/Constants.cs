﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakesAndLadders
{
    public class Constants
    {
        
        public const string GamePieceIsRightVisuals = "=>";
        public const string GamePieceIsLeftVisuals = "<=";
        public const int TurnCounterStartValue = 0;
        public const int StartRow = 10;
        public const int StartCol = 1;
        public const int CurrentPositionStartValue = 1;
        public const string OldCellStartValue = "01";
        public const bool IsDirectionRightStartValue = true;
        public const string boardMessageDefault = "Great throw!";
        public const string boardMessageLadder = "Amazing throw! You stepped on a ladder! Going to {0}";
        public const string boardMessageSnake = "Bad throw :( You stepped on a snake! Going to {0}";
        public const string progressMessage = "(you just moved from position {0} to position {1})";

        public const ConsoleColor defaultForegroundColor = ConsoleColor.Gray;
        public const ConsoleColor defaultBackgroundColor = ConsoleColor.Black;
        public const ConsoleColor defaultDiceColor = ConsoleColor.Yellow;
        public const ConsoleColor defaultPlayerColor = ConsoleColor.Blue;
        public const ConsoleColor defaultMessageColor = ConsoleColor.Red;
        public const ConsoleColor defaultBoardColor = ConsoleColor.DarkBlue;
        public const ConsoleColor defaultLadderColor = ConsoleColor.Green;
        public const ConsoleColor defaultSnakeColor = ConsoleColor.Yellow;
        public const ConsoleColor defaultProgressBarColor = ConsoleColor.Green;

        public static readonly string pad = new string(' ',40);
        public static readonly string pad2 = new string(' ', 25);
        
        public static readonly List<string> CompletedLevels = new List<string>() { "Available", "Available", "Available", "Not available" };
        public const int finishedLevelsCount = 3;
    }
}
